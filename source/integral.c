#include <stdio.h>
#include "integral.h"

double trapezoidCompound(func_t f, double a1, double a2, double precision) {
    if (a1 == a2 || precision < 1) return 0;

    double begin, end;
    if (a1<a2) {
        begin = a1;
        end = a2;
    } else {
        begin = a2;
        end = a1;
    }

    double result = 0;

    double h = (end - begin) / precision;
    double xi = 0.0;
    double xii = 0.0;
    
    xi = begin;
    xii = begin + (precision * h);
    result += (((f(xi) + f(xii))/2.0) * h);

    double t = begin + h;
    for (int i = 0; i < precision; t += h, i++){
        xi = begin + ((i - 1) * h);
        result += (f(xi) * h);
    }

    return result;
}
