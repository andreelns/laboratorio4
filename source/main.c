#include <stdio.h>
#include "integral.h"

double rampa(double t) {
    const double a = 1;
    const double b = 2;
    if (t<0) 
        return 0;
    else
        return (a*t + b);    
}

double func1(double t) {
    const double a = 1;
    const double b = 5;
    const double c = 2;
    if (t<0) 
        return 0;
    else
        return (a*t*t + b*t + c);    
}


int main(){
    printf("Teste de Integral\n\n");

    double y = trapezoidCompound(rampa, 1, 2, 100000);

    printf("Área da rampa: %lf \n\n", y);


    y = trapezoidCompound(func1, 1, 2, 100000);

    printf("Área da função1: %lf \n\n", y);

    return 0;
}
