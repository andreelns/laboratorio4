#ifndef INTEGRAL_H
#define INTEGRAL_H
typedef double (*func_t) (double);
double trapezoidCompound(func_t f, double begin, double end, double precision);
#endif
