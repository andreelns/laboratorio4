# reference https://www.embarcados.com.br/introducao-ao-makefile/

PROJECT_NAME=integral

C_SOURCE=$(wildcard ./source/*.c)
H_SOURCE=$(wildcard ./source/*.h)
OBJ=$(subst .c,.o,$(subst source,objects,$(C_SOURCE)))
 
CC=gcc
CC_FLAGS=-c         \
         -W         \
         -Wall      \
         -ansi      \
         -pedantic

RM = rm -rf
 
all: objFolder $(PROJECT_NAME)
 
$(PROJECT_NAME): $(OBJ)
	$(CC) $^ -o $@
 
./objects/%.o: ./source/%.c ./source/%.h
	$(CC) $< $(CC_FLAGS) -o $@
 
./objects/main.o: ./source/main.c $(H_SOURCE)
	$(CC) $< $(CC_FLAGS) -o $@
 
objFolder:
	@ mkdir -p objects
 
clean:
	@ $(RM) ./objects/*.o $(PROJ_NAME) *~
	@ rmdir objects